import numpy as np

class ScrambledGrid():
	def __init__(self, input_filename):
		f = open(input_filename)
		rows, _, cols = f.readline().strip()
		rows, cols = int(rows), int(cols)

		self.grid = list()
		for _ in range(rows):
			self.grid.append(f.readline().strip().split())

		self.words = list()
		line = f.readline().strip()
		while line:
			self.words.append(line)
			line = f.readline().strip()

		f.close()

		self.grid = np.array(self.grid)
		self.locations = {word:None for word in self.words}

	def horizontals(self):
		for row in self.grid:
			yield ''.join(row)

	def verticals(self):
		transposed = self.grid.T
		for transposed_col in transposed:
			yield ''.join(transposed_col)

	def increasing_diagonals(self):
		for diag in range(-self.grid.shape[0]+1,self.grid.shape[1]):
			yield ''.join(self.grid[::-1,:].diagonal(diag))

	def decreasing_diagonals(self):
		for diag in range(self.grid.shape[1]-1,-self.grid.shape[0],-1):
			yield ''.join(self.grid.diagonal(diag))


	def find_words(self):

		for index, horiz in enumerate(self.horizontals()):
			for word in self.words:
				if self.locations[word] is None:
					loc = horiz.find(word)
					if loc != -1:
						self.locations[word] = [(index, loc),(index, loc+len(word)-1)]
					else:
						loc = horiz[::-1].find(word)
						if loc != -1:
							self.locations[word] = [(index, loc+len(word)-1),(index, loc)]


		for index, vert in enumerate(self.verticals()):
			for word in self.words:
				if self.locations[word] is None:
					loc = vert.find(word)
					if loc != -1:
						self.locations[word] = [(loc, index),(loc+len(word)-1, index)]
					else:
						loc = vert[::-1].find(word)
						if loc != -1:
							self.locations[word] = [(loc+len(word)-1, index),(loc, index)]



		for index, diag in enumerate(self.increasing_diagonals()):

			origin_row = min(index, self.grid.shape[0]-1)
			origin_col = max(0, index - self.grid.shape[0]+1)

			for word in self.words:
				if self.locations[word] is None:
					loc = diag.find(word)
					if loc != -1:
						if index < self.grid.shape[0]:
							self.locations[word] = [(               origin_row - loc , origin_col + loc              ),
										  			(-len(word)+1 + origin_row - loc , origin_col + loc + len(word)-1)]
					else:
						loc = diag[::-1].find(word)
						if loc != -1:
							self.locations[word] = [(-len(word)+1 + origin_row - loc , origin_col + loc + len(word)-1),
													(               origin_row - loc , origin_col + loc              )]


		for index, diag in enumerate(self.decreasing_diagonals()):

			origin_row = max(0, index - self.grid.shape[1]+1)
			origin_col = min(0, self.grid.shape[1]-1 - index)

			for word in self.words:
				if self.locations[word] is None:
					loc = diag.find(word)
					if loc != -1:
						if index < self.grid.shape[0]:
							self.locations[word] = [(              origin_row + loc , origin_col + loc              ),
										  			(len(word)-1 + origin_row + loc , origin_col + loc + len(word)-1)]
					else:
						loc = diag[::-1].find(word)
						if loc != -1:
							self.locations[word] = [(len(word)-1 + origin_row + loc , origin_col + loc + len(word)-1),
													(              origin_row + loc , origin_col + loc              )]

	def print_results(self):
		for word in self.words:
			start, end = self.locations[word]
			print(word, str(start[0])+':'+str(start[1]), str(end[0])+':'+str(end[1]))