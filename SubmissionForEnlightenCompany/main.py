from ScrambledGrid import ScrambledGrid
import sys


def main():
	if len(sys.argv) != 2:
		print("Usage: python3 challenge.py <input_filename>")
		sys.exit(1)

	input_filename = sys.argv[1]

	puzzle = ScrambledGrid(input_filename)
	puzzle.find_words()
	puzzle.print_results()


if __name__ == '__main__':
	main()