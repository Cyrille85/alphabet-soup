*******************
Author: 
Date: 18/02/2021
*******************

***Objective***
Finding words within a grid of characters: Python program challenge.


***Dependencies***

	-Python standard library
	-Numpy library


***Running***

For introducing a new grid:
	python3 challenge.py <input_filename>

For testing with two already given test files:
	python3 test1.py
	python3 test2.py

