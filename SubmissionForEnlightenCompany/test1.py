from ScrambledGrid import ScrambledGrid


content = \
"""5x5
H A S D F
G E Y B H
J K L Z X
C V B L N
G O O D O
HELLO
GOOD
BYE
"""
print("----------\nInput:\n----------\n")
print(content)
with open("test1.txt",'w') as f:
	f.write(content)

puzzle = ScrambledGrid("test1.txt")
puzzle.find_words()
print("\n----------\nOutput:\n----------\n")
puzzle.print_results()
print("\n----------\nExpected Output:\n----------\n")
print(
"""HELLO 0:0 4:4
GOOD 4:0 4:3
BYE 1:3 1:1
""")