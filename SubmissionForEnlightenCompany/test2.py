from ScrambledGrid import ScrambledGrid


content = \
"""3x3
A B C
D E F
G H I
ABC
AEI
"""
print("----------\nInput:\n----------\n")
print(content)
with open("test2.txt",'w') as f:
	f.write(content)

puzzle = ScrambledGrid("test2.txt")
puzzle.find_words()
print("\n----------\nOutput:\n----------\n")
puzzle.print_results()
print("\n----------\nExpected Output:\n----------\n")
print(
"""ABC 0:0 0:2
AEI 0:0 2:2
""")
